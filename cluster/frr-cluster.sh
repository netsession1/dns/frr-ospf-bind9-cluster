#!/bin/bash
echo configure FRR for OSPF Cluster $CLUSTER_PRI_IP/$CLUSTER_SEC_IP
OUTPUT_CONFIG=/etc/frr/frr.conf

echo zebra=yes >> /etc/frr/daemons
echo ospfd=yes >> /etc/frr/daemons

INTERFACE_ID="${INTERFACE:-ens192}"
ROUTER_ID=`ip addr show dev $INTERFACE_ID | awk '$1=="inet"{split($2,a,"/"); print(a[1])}'`

echo using ospf router-id $ROUTER_ID

cat /config/frr.conf-cluster > $OUTPUT_CONFIG

echo """
interface $INTERFACE_ID
 ip ospf area 0
 ip ospf dead-interval 2
 ip ospf hello-interval 1
""" >> $OUTPUT_CONFIG


if [ ! -z ${CLUSTER_PRI_IP+x} ]
then
  echo Useing primary cluster IP $CLUSTER_PRI_IP
  ip addr add ${CLUSTER_PRI_IP} dev lo
  echo """
  ip prefix-list cluster-pri permit ${CLUSTER_PRI_IP}/32
  route-map cluster permit 10
       match ip address prefix-list cluster-pri
       set metric 200
  route-map loadbalance permit 10
       match ip address prefix-list cluster-pri
  """ >> $OUTPUT_CONFIG
fi

if [ ! -z ${CLUSTER_SEC_IP+x} ]
then
  echo Useing secondary cluster IP $CLUSTER_SEC_IP
  ip addr add ${CLUSTER_SEC_IP} dev lo
  echo """
  ip prefix-list cluster-sec permit ${CLUSTER_SEC_IP}/32
  route-map cluster permit 20
       match ip address prefix-list cluster-sec
       set metric 400
  route-map loadbalance permit 20
       match ip address prefix-list cluster-sec
  """ >> $OUTPUT_CONFIG
fi

if [ ! -z ${LOADBALANCE+x} ]
then
echo """
router ospf
 ospf router-id $ROUTER_ID
 redistribute connected metric-type 2 route-map loadbalance
 clear ip ospf process
""" >> $OUTPUT_CONFIG
else
echo """
router ospf
 ospf router-id $ROUTER_ID
 redistribute connected metric-type 2 route-map cluster
 clear ip ospf process
""" >> $OUTPUT_CONFIG
fi
/usr/lib/frr/docker-start
