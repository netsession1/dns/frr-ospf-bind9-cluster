# frr

FRR routing service für OSPF based clusters
https://frrouting.org/

## Usage

Dieser Container wird in Verbindung mit einem Dienst wie DNS verwendet. Alle Clustermitglieder geben dieselben Cluster-IPs an OSPF weiter. Der Core Switch oder Firewall sorgt dann für einen Lastausgleich der Anfragen mit Equal Cost Multipath Routing. 

Cisco Config:
```
ip routing

interface Vlan4000
 no ip redirects
 ip ospf dead-interval 2
 ip ospf hello-interval 1

router ospf 1716
 passive-interface default
 no passive-interface Vlan4000
 network 10.0.0.0 0.255.255.255 area 0
```

## caveats

Die Cluster-IPs werden nicht vom Hostsystem entfernt, wenn der Container gelöscht oder gestoppt wird.